#!/bin/bash

dir=$(dirname $0)

source $dir/ENV

function enable_ssh_with_root(){
  systemctl enable sshd
  systemctl start sshd
  control sshd-permit-root-login enabled
  systemctl restart sshd
}

function upgrade_system() {
  apt-get update -qq -y
  apt-get dist-upgrade -qq -y
  update-kernel -y
}

function set_up_vmbr0(){
  rm -rf /etc/net/ifaces/*

  mkdir /etc/net/ifaces/lo
  echo '127.0.0.1/8' > /etc/net/ifaces/lo/ipv4address
  echo '# Having lo immortal by default is good.' > /etc/net/ifaces/lo/options
  echo 'DISABLED=no' > /etc/net/ifaces/lo/options

  mkdir /etc/net/ifaces/vmbr0
  cat << EOF > /etc/net/ifaces/vmbr0/options
TYPE=bri
VLAN_AWARE=yes
VIDS=2-2048
ONBOOT=yes
DISABLED=no
NM_CONTROLLED=no
CONFIG_WIRELESS=no
CONFIG_IPV4=yes
CONFIG_IPV6=no
BOOTPROTO=static
HOST="${ETH}"
EOF

  mkdir /etc/net/ifaces/${ETH}
  cat << EOF > /etc/net/ifaces/${ETH}/options
TYPE=eth
ONBOOT=yes
DISABLED=no
NM_CONTROLLED=no
CONFIG_WIRELESS=no
CONFIG_IPV4=yes
CONFIG_IPV6=no
BOOTPROTO=static
EOF

  systemctl disable NetworkManager
  systemctl stop NetworkManager

  echo ${IP_MASK} > /etc/net/ifaces/vmbr0/ipv4address
  echo "default via ${GATEWAY}" > /etc/net/ifaces/vmbr0/ipv4route

  mac=`ip link show ${ETH} | grep 'link/ether' | awk {'print $2'}`
  echo "address ${mac}" > /etc/net/ifaces/vmbr0/iplink
  service network restart
}

function set_up_hosts_settings() {
  SUFFIX=`echo ${IP_MASK} | awk -F '/' {'print $1'} | awk -F '.' {'print $3 "-" $4'}`
  MACHINE_NAME="${HOSTNAME}-${SUFFIX}"
  hostnamectl set-hostname ${MACHINE_NAME}
  IP=`ip a | grep vmbr0 | tail -1 | awk {'print $2'} | awk -F '/' {'print $1'}`
  HOST=`hostname`
  echo "${HOST}.${DOMAIN}" >/etc/hostname
  echo '127.0.0.1 localhost' >/etc/hosts
  echo "${IP} ${HOST}.${DOMAIN} ${HOST}" >>/etc/hosts
}

function install_and_start_pve() {
  apt-get install -qq -y pve-manager kernel-modules-zfs-un-def cifs-utils sendmail ansible python python-module-yaml python-module-jinja2 python-modules-json sendmail
  systemctl start pve-cluster
  systemctl enable pve-cluster
  systemctl start lxc lxc-net lxc-monitord pvedaemon pve-firewall pvestatd pve-ha-lrm pve-ha-crm spiceproxy pveproxy
  systemctl enable corosync lxc lxc-net lxc-monitord pve-cluster pvedaemon pve-firewall pvestatd pve-ha-lrm pve-ha-crm spiceproxy pveproxy pve-guests
}

set_up_vmbr0
upgrade_system
set_up_hosts_settings
enable_ssh_with_root
install_and_start_pve
