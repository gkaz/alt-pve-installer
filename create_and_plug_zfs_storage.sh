#!/bin/bash
dir=$(dirname $0)
source $dir/ENV

# by default - 4
os_disk_new_partition_number="4"

function fix_mdadm() {
  echo '#!/bin/sh' >/etc/rc.d/rc.local
  echo 'sleep 10s' >>/etc/rc.d/rc.local
  echo '/opt/fix_mdadm.sh' >>/etc/rc.d/rc.local
  echo 'exit 0' >>/etc/rc.d/rc.local
  chmod +x /etc/rc.d/rc.local

  cat <<'EOF' >/opt/fix_mdadm.sh
#/bin/bash
sleep 10s
/sbin/mdadm --stop /dev/md127
/bin/echo 1 >/sys/module/raid0/parameters/default_layout
/sbin/mdadm --assemble --scan
EOF

  chmod +x /opt/fix_mdadm.sh

}

function create_zfs() {

  fdisk ${OS_DISK} <<EOF
n



w
EOF

  if [ ! -z $DATA_DISK ]; then

    wipefs -af ${DATA_DISK}
    fdisk ${DATA_DISK} <<EOF
g
n



Y
w
EOF

  fi

  if [ "$NVME" == "yes" ]; then
    wipefs -af "${OS_DISK}p${os_disk_new_partition_number}"
  else
    wipefs -af "${OS_DISK}${os_disk_new_partition_number}"
  fi

  echo 'zfs' >>/etc/modules-load.d/zfs.conf
  /sbin/modprobe zfs

  if [ ! -z $DATA_DISK ]; then

    if [ "$NVME" == "yes" ]; then
      wipefs -af "${DATA_DISK}p1"

      mdadm --create /dev/md0 -l 0 -n 2 "${OS_DISK}p${os_disk_new_partition_number}" "${DATA_DISK}p1"

      if [ $? -ne 0 ]; then
        echo 1 >/sys/module/raid0/parameters/default_layout
        mdadm --create /dev/md0 -l 0 -n 2 "${OS_DISK}p${os_disk_new_partition_number}" "${DATA_DISK}p1"
      fi

    else

      wipefs -af "${DATA_DISK}1"

      mdadm --create /dev/md0 -l 0 -n 2 "${OS_DISK}${os_disk_new_partition_number}" "${DATA_DISK}1"

      if [ $? -ne 0 ]; then
        echo 1 >/sys/module/raid0/parameters/default_layout
        mdadm --create /dev/md0 -l 0 -n 2 "${OS_DISK}${os_disk_new_partition_number}" "${DATA_DISK}1"
      fi

    fi

    zpool create zfspool /dev/md0 -f

  else

    if [ "$NVME" == "yes" ]; then
      zpool create zfspool "${OS_DISK}p${os_disk_new_partition_number}" -f
    else

      zpool create zfspool "${OS_DISK}${os_disk_new_partition_number}" -f
    fi
  fi

  zfs set compression=off zfspool
  zfs set sync=disabled zfspool

}

fix_mdadm
create_zfs
pvesm add zfspool vmstorage --pool zfspool --content images,rootdir
