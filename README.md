# Alt Pve Installer

alt-pve-installer

## Getting started

### Install Alt Linux From ISO image

https://mirror.yandex.ru/altlinux-starterkits/x86_64/release/alt-p10-gnome3-20220312-x86_64.iso

### From installed OS

 `su -`

 `cd /opt`

 `wget https://gitlab.com/gkaz/alt-pve-installer/-/raw/main/update.sh -O update.sh`
 
 `chmod +x update.sh`

 `./update.sh`
 
if need - edit params in file:

 `./ENV`

then

 `./install.sh`
 
then after reboot...

### Open WEB console

 `firefox https://localhost:8006`
 
