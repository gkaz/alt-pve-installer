#!/bin/bash

function mount_cifs_storages(){
  pvesm add cifs iso --server 172.16.254.123 --share disk1 --username root --password --content iso
  pvesm add cifs disk1 --server 192.168.10.126 --share disk1 --username root --password --content backup
  pvesm add cifs disk2 --server 192.168.10.126 --share disk2 --username root --password --content backup
  pvesm add cifs disk3 --server 192.168.10.126 --share disk3 --username root --password --content backup
  pvesm add cifs disk4 --server 192.168.10.126 --share disk4 --username root --password --content backup
}

mount_cifs_storages