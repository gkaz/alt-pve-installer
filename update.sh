#!/bin/bash
wget https://gitlab.com/gkaz/alt-pve-installer/-/raw/main/update.sh -O update.sh
wget https://gitlab.com/gkaz/alt-pve-installer/-/raw/main/install.sh -O install.sh
wget https://gitlab.com/gkaz/alt-pve-installer/-/raw/main/install-pve.sh -O install-pve.sh
wget https://gitlab.com/gkaz/alt-pve-installer/-/raw/main/create_and_plug_zfs_storage.sh -O create_and_plug_zfs_storage.sh
wget https://gitlab.com/gkaz/alt-pve-installer/-/raw/main/plug_cifs_storages.sh -O plug_cifs_storages.sh
wget https://gitlab.com/gkaz/alt-pve-installer/-/raw/main/ENV -O ENV
chmod +x *.sh