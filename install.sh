#!/bin/bash
host_base='node'
domain_base='local'

dir=$(dirname $0)

function create_env() {
  cp "${dir}/ENV" "${dir}/ENV_backup_$(date +%s)"

  ip_grep_base=$(ip ro | grep default | awk {'print $3'} | awk -F '.' {'print $1"."$2'})
  iface=$(ip ro | grep default | awk {'print $5'})
  ip_and_mask=$(ip a | grep $ip_grep_base | awk {'print $2'})
  gw=$(ip ro | grep default | awk {'print $3'})
  dns=$gw

  #detect hdd or nvme
  os_disk=$(df -l | grep "/$" | awk {'print $1'} | awk -F '/' {'print "/" $2 "/" $3'} | grep -o ........ | head -1)
  is_nvme="no"
  if [ "$(df -l | grep "/$" | awk {'print $1'} | awk -F '/' {'print $3'} | grep -o .... | head -1)" == "nvme" ]; then
    is_nvme="yes"
    os_disk=$(df -l | grep "/$" | awk {'print $1'} | awk -F '/' {'print "/" $2 "/" $3'} | grep -o ............ | head -1)
  fi

  os_disk_short_name=$(echo $os_disk | awk -F '/' {'print $3'})
  data_disk_short_name=$(ls /dev/ | egrep -o '^sd.|^nvme...' | uniq | grep -v "$os_disk_short_name" | head -1)
  data_disk=''

  if [ ! -z "$data_disk_short_name" ]; then
    data_disk="/dev/$data_disk_short_name"
  fi

  cat <<EOF >"${dir}/ENV"
IP_MASK='${ip_and_mask}'
GATEWAY='${gw}'
DNS='${dns}'
HOSTNAME='${host_base}'
DOMAIN='${domain_base}'
ETH='${iface}'
NVME='$is_nvme'
OS_DISK='$os_disk'
DATA_DISK='$data_disk'
EOF
}

#create_env
echo '####### PLEASE CHECK ENV #######'
cat ${dir}/ENV
echo '####### TYPE yes if ALL OK #######'

read ansv

if [ "$ansv" == 'yes' ]; then
  ${dir}/install-pve.sh
  ${dir}/create_and_plug_zfs_storage.sh
  ${dir}/plug_cifs_storages.sh
  echo "COMPLETE!"
  exit 0
fi

echo "REJECTED BY USER"
exit 1
